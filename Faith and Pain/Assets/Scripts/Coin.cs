﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Item, IPickable
{

    public override void Awake()
    {
        base.Awake();
        if (SaveSystem.IsItemInInventory(ID))
        {
            OnPickup();
        }
    }
    public override void OnPickup()
    {
        base.OnPickup();

        var uiManager = FindObjectOfType<UIManager>();

        if (uiManager != null)
            uiManager.AddScore(1);

        gameObject.SetActive(false);
    }
}
