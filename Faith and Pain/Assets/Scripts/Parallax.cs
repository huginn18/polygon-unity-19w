﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
	public Transform cameraTransform;
	public float parallaxStrenght;
	public MeshRenderer meshRenderer;

	private const float PARALLAX_MODIFIER = 20;

	private Material material;

    // Start is called before the first frame update
    void Start()
    {
		material = meshRenderer.material;
    }

    // Update is called once per frame
    void Update()
    {
		material.SetTextureOffset("_MainTex", new Vector2(cameraTransform.position.x*parallaxStrenght/PARALLAX_MODIFIER, 0));
    }
}
