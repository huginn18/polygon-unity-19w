﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
	[SerializeField] private Rigidbody2D rb;
	[SerializeField] private ParticleSystem prt;
	public int ID;

    // Start is called before the first frame update
    void Awake()
    {
		ID = IDDispenser.GetID();
		bool alive = !SaveSystem.IsEnemyKilled(ID);
		this.gameObject.SetActive(alive);
		if (alive)
		{
			EnemyData data = SaveSystem.GetEnemyData(ID);

			if (data != null)
			{
				transform.position = data.Position;
				transform.localScale = data.Scale;
				transform.rotation = data.Rotation;
			}
		}
		SaveSystem.Enemies.Add(this);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		Move();
	}

	void Move()
	{
		rb.velocity = new Vector2(-1, rb.velocity.y);
	}

	public void Kill()
	{
		Debug.Log("enemy killed");
		ParticleSystem tempObj = Instantiate(prt);
		tempObj.transform.position = this.transform.position;
		gameObject.SetActive(false);
		SaveSystem.AddKilledEnemy(ID);
	}
}
