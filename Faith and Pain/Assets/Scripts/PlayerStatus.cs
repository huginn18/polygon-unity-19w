﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus
{

	public int InitialHP = 5;
	private int _playerHP = 0;

	public int GetPlayerHP()
	{
		return _playerHP;
	}

	public void ResetPlayerHP()
	{
		_playerHP = InitialHP;
		PlayerHPChanged();
	}

	public void SetPlayerHP(int value)
	{
		_playerHP = value;
		PlayerHPChanged();
	}

	public void TakeDamage(int damage)
	{
		_playerHP -= damage; // _playerHP = _playerHP - damage;
		PlayerHPChanged();
	}

	public void Heal(int amount)
	{
		_playerHP += amount; // _playerHP = _playerHP - damage;
		PlayerHPChanged();
	}

	public delegate void PlayerHPChange(int hp);
	public event PlayerHPChange OnPlayerHPChange;

	public void PlayerHPChanged()
	{
		if (OnPlayerHPChange != null)
		{
			OnPlayerHPChange.Invoke(_playerHP);
		}
		
		if(_playerHP <= 0 && OnPlayerDeath != null)
		{
			KillPlayer();
		}
	}

	public delegate void PlayerDied();
	public event PlayerDied OnPlayerDeath;
	public void KillPlayer()
	{
		OnPlayerDeath.Invoke();
	}


}
