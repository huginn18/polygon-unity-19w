﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public static class SaveSystem
{

	private static string SaveFolderPath = Application.persistentDataPath;

	private static SaveData SaveData;

	public static List<int> PickedItemsList = new List<int>();

	public static List<int> OpenedDoorsList = new List<int>();

	private static List<int> KilledEnemies = new List<int>();

	public static List<EnemyController> Enemies = new List<EnemyController>();

	public static void SaveGameToFile(string filename)
	{
		SaveData = new SaveData();
		Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

		SaveData.PlayerPosition = playerTransform.position;
		SaveData.PlayerScale = playerTransform.localScale;
		SaveData.PlayerRotation = playerTransform.rotation;

		SaveData.PlayerHP = GameObject.FindObjectOfType<PlayerController>().status.GetPlayerHP();

		SaveData.Score = GameObject.FindObjectOfType<UIManager>().GetScore();

		SaveData.DisabledItems = PickedItemsList.ToArray();

		SaveData.OpenedDoors = OpenedDoorsList.ToArray();

		Item InventoryItem = GameObject.FindObjectOfType<Inventory>().item;

		if (InventoryItem != null)
		{
			SaveData.InventoryItemID = InventoryItem.ID;
		}
		else
		{
			SaveData.InventoryItemID = -1;
		}

		List<EnemyData> enemyDatas = new List<EnemyData>();


		for (int i = 0; i < Enemies.Count; i++)
		{
			int Id = Enemies[i].ID;

			EnemyData temp = new EnemyData();

			temp.ID = Id;

			if (KilledEnemies.Contains(Id))
			{
				temp.Killed = true;
			}

			temp.Position = Enemies[i].transform.position;
			temp.Rotation = Enemies[i].transform.rotation;
			temp.Scale = Enemies[i].transform.localScale;

			enemyDatas.Add(temp);
		}

		SaveData.Enemies = enemyDatas.ToArray();

		string json = JsonUtility.ToJson(SaveData, true);

		string filePath = SaveFolderPath + "/" + filename + ".FAP";

		if (File.Exists(filePath))
		{
			File.Delete(filePath);
		}

		if (File.Exists(filePath))
		{
			Debug.LogError("Cannot delete file " + filePath);
			return;
		}

		File.WriteAllText(filePath, json);

		Debug.Log("Saved: " + json + "\nto " + filePath);

	}

	public static void LoadGameFromFile(string filename)
	{
		string filePath = SaveFolderPath + "/" + filename + ".FAP";

		if (!File.Exists(filePath))
		{
			Debug.LogError("No such file as: " + filePath);
			return;
		}

		string json = File.ReadAllText(filePath);

		Debug.Log("Loaded data: " + json);

		SaveData = JsonUtility.FromJson<SaveData>(json);

		if (SaveData.DisabledItems == null)
		{
			PickedItemsList = new List<int>();
		}
		else
		{
			PickedItemsList = SaveData.DisabledItems.ToList();
		}

		if (SaveData.OpenedDoors == null)
		{
			OpenedDoorsList = new List<int>();
		}
		else
		{
			OpenedDoorsList = SaveData.OpenedDoors.ToList();
		}

		KilledEnemies = new List<int>();

		Enemies = new List<EnemyController>();

		if (SaveData.Enemies != null)
		{
			foreach (var enemy in SaveData.Enemies)
			{
				if (enemy.Killed)
				{
					KilledEnemies.Add(enemy.ID);
				}
			}
		}

		IDDispenser.ResetID();

		SceneManager.LoadScene(SaveData.SceneIndex, LoadSceneMode.Single);

		SceneManager.sceneLoaded += SceneManager_sceneLoaded;

	}

	private static void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
	{
		//GameObject.FindObjectOfType<Inventory>().RemoveItem();

		Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

		playerTransform.position = SaveData.PlayerPosition;
		playerTransform.localScale = SaveData.PlayerScale;
		playerTransform.rotation = SaveData.PlayerRotation;

		GameObject.FindObjectOfType<PlayerController>().status.SetPlayerHP(SaveData.PlayerHP);

		Debug.Log("Load complete");
	}

	public static bool IsItemDisabled(int id)
	{
		if (SaveData == null)
		{
			return false;
		}
		for (int i = 0; i < SaveData.DisabledItems.Length; i++)
		{
			if (SaveData.DisabledItems[i] == id)
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsDoorOpened(int id)
	{
		if (SaveData == null)
		{
			return false;
		}
		for (int i = 0; i < SaveData.OpenedDoors.Length; i++)
		{
			if (SaveData.OpenedDoors[i] == id)
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsItemInInventory(int id)
	{
		if (SaveData == null)
		{
			return false;
		}
		return SaveData.InventoryItemID == id;
	}

	public static bool IsEnemyKilled(int id)
	{
		if (SaveData == null)
		{
			return false;
		}
		if (SaveData.Enemies == null)
		{
			return false;
		}
		for (int i = 0; i < SaveData.Enemies.Length; i++)
		{
			if (SaveData.Enemies[i].ID == id && SaveData.Enemies[i].Killed)
			{
				return true;
			}
		}
		return false;
	}

	public static EnemyData GetEnemyData(int id)
	{
		if (SaveData == null)
		{
			return null;
		}
		if (SaveData.Enemies == null)
		{
			return null;
		}
		for (int i = 0; i < SaveData.Enemies.Length; i++)
		{
			if (SaveData.Enemies[i].ID == id)
			{
				return SaveData.Enemies[i];
			}
		}
		return null;
	}

	public static void AddKilledEnemy(int id)
	{
		if (!KilledEnemies.Contains(id))
		{
			KilledEnemies.Add(id);
		}
	}

	public static int GetInventoryItemID()
	{
		if (SaveData == null)
		{
			return -1;
		}
		return SaveData.InventoryItemID;
	}

}
