﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    [SerializeField]
    Transform destination;

    bool isActive = true;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isActive && collision.tag == "Player")
        {
            Teleport otherTeleport = destination.GetComponent<Teleport>();
            if (otherTeleport != null)
                otherTeleport.isActive = false;

            collision.gameObject.transform.position = destination.position;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isActive = true;
    }
}
