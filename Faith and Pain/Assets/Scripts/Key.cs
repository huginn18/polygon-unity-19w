﻿using UnityEngine;
using System;

public class Key : Item, IPickable
{
    public override void Awake()
    {
        base.Awake();
        if (SaveSystem.IsItemInInventory(ID))
        {
            OnPickup();
        }
        if (SaveSystem.GetInventoryItemID() == ID)
        {
            Inventory inventory = GameObject.FindObjectOfType<Inventory>();
            inventory.item = this;
        }
    }

    public override void OnPickup()
    {
        base.OnPickup();

        var uiManager = FindObjectOfType<UIManager>();
        uiManager.SetItem(GetComponent<SpriteRenderer>());
        gameObject.SetActive(false);
        isPickable = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isPickable)
            return;

        if (collision.tag == "Player")
        {
            Inventory inventory = collision.gameObject.GetComponent<Inventory>();

            if (inventory != null)
            {
                if (inventory.item == null)
                {
                    inventory.item = this;
                    OnPickup();
                }
                else
                {
                    Debug.LogWarning("Inventory is full!");
                }
            }
        }
    }
}
