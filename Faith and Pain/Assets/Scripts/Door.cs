﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private int ID;
    private void Awake()
    {
        ID = IDDispenser.GetID();
        if (SaveSystem.IsDoorOpened(ID))
        {
            this.gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            Inventory inventory = collision.gameObject.GetComponent<Inventory>();
            if (inventory != null && inventory.item is Key)
            {
                inventory.RemoveItem();
                SaveSystem.OpenedDoorsList.Add(ID);
                gameObject.SetActive(false);
            }
        }
    }
}
