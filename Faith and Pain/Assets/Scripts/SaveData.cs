﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public int SceneIndex;

    public Vector3 PlayerPosition;
    public Vector3 PlayerScale;
    public Quaternion PlayerRotation;

    public int Score;

    public int PlayerHP;

    public int[] DisabledItems;

    public int[] OpenedDoors;

    public int InventoryItemID;

    public EnemyData[] Enemies;
}
