﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IDDispenser
{
    private static int ID = 0;

    public static int GetID()
    {
        ID++;
        return ID;
    } 

    public static void ResetID()
    {
        ID = 0;
    }
}
