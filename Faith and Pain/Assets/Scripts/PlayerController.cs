﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody2D))] //this tells unity to warn us if there is no Rigidbody2D Component, it is required for physics
public class PlayerController : MonoBehaviour //we are inheriting from MonoBehaviour, this makes unity check if this code has unity specific functions such as Update and Start, and calls them accordingly. Try avoiding it if you can, as it is a huge performance hit
{
	//a region allows us to hide code in a single line, for easier scrolling through it
	#region public Variables

	[Header("Jump settings")] //just some bold text visible in the inspector
	[Range(1, 10)] [Tooltip("How high in Unity units do we want to jump?")]
	public float JumpHeight = 3f;
	[Range(0.1f, 10)] [Tooltip("How long do we want it to take, to reach the highest point?")]
	public float JumpTime = 1f;
	[Range(1, 10)] [Tooltip("How far should a player be able to jump?")]
	public float JumpLength = 6f;
	[Tooltip("Reference to player's Rigidbody2D Component.")]
	public Rigidbody2D rb; //it controls our object physic properties such as mass and gravity scale
	#endregion

	public Collider2D JumpCollider;
	[SerializeField] Collider2D MainCollider;
	private float JumpVelocity; //Initial Velocity we need to have to compelete the jump

	public int initialHP;
    public Animator animator;

	public PlayerStatus status;

	public void PlayerDeath()
	{
		Debug.LogWarning("Welcome to dark souls");
		//PlayerStatus.ResetPlayerHP();
		gameObject.SetActive(false);
		Destroy(gameObject);
	}

	private void Awake()
	{
		status = new PlayerStatus();
	}

	// Start is called before the first frame update
	void Start()
    {
		if(rb == null) //if we forgot to assign the Rigidbody2D Component to the rb variable in the inspector...
		{
			Debug.LogWarning("No rigidbody was set, attempting to find one.");
			rb = this.gameObject.GetComponent<Rigidbody2D>(); //...the code will assign it for us
		}	

		status.OnPlayerDeath += PlayerDeath;

		status.InitialHP = initialHP;
		status.ResetPlayerHP();

		status.OnPlayerHPChange += (int x) => Debug.Log("you hava " + x + " hp left");
	}

    // Update is called once per frame, It's FPS dependent
    void Update()
    {
		//we should try to keep our code clean (avoid so many comments), so I moved our code to separate functions
		CalculateJumpParameters(); //do all the maths

		if (IsTouchingGround())
		{
			if (Input.GetButtonDown("Jump")) //this condition is incomplete, it let's us jump even when not touching the ground, we will fix it soon enough
			{
				Jump();
			}
			Move(); //make the player move
		}
		else if(CanMoveInAir())
		{
			Move();
		}	

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			GameObject.FindObjectOfType<UIManager>().ToggleSavePanel();
            Time.timeScale = -Time.timeScale + 1f;
		}

        if (Input.GetKeyDown(KeyCode.G))
        {
            Inventory inventory = GetComponent<Inventory>();
            inventory.DropItem(transform.position);

            Debug.Log("Item dropped");
        }

    }

	#region Movement functions
	//a region allows us to hide code in a single line

	/// <summary>
	/// Use variables set in the inspector to calculate gravity scale and jump velocity
	/// </summary>
	private void CalculateJumpParameters()
	{
		float gravity = (2f * JumpHeight) / Mathf.Pow(JumpTime, 2f);
		JumpVelocity = gravity * JumpTime;
		//We use Rigidbody2D Component as our interface with the physics engine
		//We set a gravity scale every displayed frame, but the gravity is only applied once per physics update, 50 times a second by default (once every 20 miliseconds)
		//We want to bo independant from Project's gravity set in Unity, we need to calculate gravity scale based on our calculated gravity and project settings
		rb.gravityScale = gravity / -Physics.gravity.y; 
	}

	/// <summary>
	/// Use variables set in the inspector and Player input to calculate and apply horizontal movement
	/// </summary>
	private void Move()
	{
		float input = Input.GetAxis("Horizontal"); //User input ranging from -1 to +1, works with keyboard and console controllers

        animator.SetBool("IsWalk", input != 0);
        
        float SidewaysVelocity = (JumpLength * input) / (2f * JumpTime);

        animator.SetFloat("Speed", Mathf.Abs( SidewaysVelocity/8f));

		//we only want to change the horizontal motion (the X value of the vector2D)


		rb.velocity = new Vector2(SidewaysVelocity, rb.velocity.y);
	}

	/// <summary>
	/// Apply vertical movement
	/// </summary>
	private void Jump()
	{
		//when jumping we only want to  change to vertical motion (the Y value of the vector2D)
		rb.velocity = new Vector2(rb.velocity.x, JumpVelocity);
	}

	/// <summary>
	/// Is the Player touching the ground
	/// </summary>
	/// <returns></returns>
	private bool IsTouchingGround()
	{
		List<Collider2D> others = new List<Collider2D>();
		JumpCollider.GetContacts(others);

		foreach (Collider2D collider in others)
		{
			if (collider.tag == "Environment")
			{
				return true;
			}
		}

		return false;
	}

	private bool CanMoveInAir()
	{
		List<Collider2D> others = new List<Collider2D>();
		MainCollider.GetContacts(others);

		foreach (Collider2D collider in others)
		{
			if (collider.gameObject.name != "JumpTrigger")
			{
				return false;
			}
		}

		return true;
	}

	#endregion

}
