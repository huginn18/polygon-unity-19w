﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public Item item;

    public void RemoveItem()
    {
        //Destroy(item.gameObject);
        item = null;

        var uiManager = FindObjectOfType<UIManager>();
        uiManager.SetItem(null);
    }

    public void DropItem(Vector3 dropPosition)
    {
        if (item != null)
        {
            item.transform.position = dropPosition;
            item.gameObject.SetActive(true);
            item = null;

            var uiManager = FindObjectOfType<UIManager>();
            uiManager.SetItem(null);
        }
    }
}
