﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public int ID;
    public bool isPickable = true;

    public virtual void OnPickup()
    {
        Debug.Log("item picked up");
        if (!SaveSystem.PickedItemsList.Contains(ID))         
        {
            SaveSystem.PickedItemsList.Add(ID);
        }
    }

    public virtual void Awake()
    {
        ID = IDDispenser.GetID();
        this.gameObject.SetActive(!SaveSystem.IsItemDisabled(ID));

        Debug.Log("item " + ID + " loaded, active: " + this.gameObject.activeSelf);

        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            OnPickup();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isPickable = true;
    }
}
