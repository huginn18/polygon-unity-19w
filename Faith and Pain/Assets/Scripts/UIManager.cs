﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreUI;
    [SerializeField] Image item;
    [SerializeField] Image SavePanel;

    int score = 0;

    private void Start()
    {
        scoreUI.text = "Score: " + score;
    }

    public int GetScore()
    {
        return score;
    }

    public void SetScore(int value)
    {
        score = value;
    }

    public void AddScore(int value)
    {
        score += value;
        scoreUI.text = "Score: " + score;
    }

    public void SetItem(SpriteRenderer newItem)
    {
        if (newItem != null)
        {
            item.sprite = newItem.sprite;
            item.color = newItem.color;
            item.enabled = true;
        }
        else
        {
            item.enabled = false;
        }
    }

    public void ButtonSaveGame(string name)
    {
        SaveSystem.SaveGameToFile(name);
    }

    public void ButtonLoadGame(string name)
    {
        SaveSystem.LoadGameFromFile(name);
    }

    public void HideSavePanel()
    {
        SavePanel.gameObject.SetActive(false);
    }

    public void ShowSavePanel()
    {
        SavePanel.gameObject.SetActive(true);
    }

    public void ToggleSavePanel()
    {
        SavePanel.gameObject.SetActive(!SavePanel.gameObject.activeSelf);
    }
}
