﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyData
{
    public int ID;
    public Vector3 Position;
    public Vector3 Scale;
    public Quaternion Rotation;
    public bool Killed;
}
